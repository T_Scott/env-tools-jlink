# Reset the device
add_custom_target(reset COMMAND JLinkExe -device ${DEVICE} -if swd -speed 4000 -CommanderScript ${CMAKE_CURRENT_LIST_DIR}/commands/reset.txt)

# Erase the device
add_custom_target(erase COMMAND JLinkExe -device ${DEVICE} -if swd -speed 4000 -CommanderScript ${CMAKE_CURRENT_LIST_DIR}/commands/erase.txt)

# Write flash
configure_file(${CMAKE_CURRENT_LIST_DIR}/commands/flash.txt ${CMAKE_CURRENT_BINARY_DIR}/jlink/flash.txt)
add_custom_target(flash DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_OUTPUT}.elf COMMAND JLinkExe -device ${DEVICE} -if swd -speed 4000 -CommanderScript ${CMAKE_CURRENT_BINARY_DIR}/jlink/flash.txt)

# Run debug server
configure_file(${CMAKE_CURRENT_LIST_DIR}/commands/gdb.txt ${CMAKE_CURRENT_BINARY_DIR}/jlink/gdb.txt)
add_custom_target(debug DEPENDS ${PROJECT_OUTPUT}.elf COMMAND arm-none-eabi-gdb --se=${PROJECT_OUTPUT}.elf --command=${CMAKE_CURRENT_BINARY_DIR}/jlink/gdb.txt)
add_custom_target(debug-server DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_OUTPUT}.elf COMMAND JLinkGDBServer -device ${DEVICE} -if swd)


